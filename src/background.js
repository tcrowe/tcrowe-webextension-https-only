let { get } = require("./storage");

let httpStartPattern = /^http:\/\//;
let httpsStart = "https://";
let httpFilter = { urls: ["http://*/*"] };

// ignore patterns
let localhostPatterns = [
  "http://127.",
  "http://localhost",
  "http://192.",
  "http://10."
];

function before_request(req) {
  let enabled = get("enabled");

  if (enabled === false) {
    // the extension is disabled, skip
    return;
  }

  let { url } = req;

  if (url.startsWith(httpsStart)) {
    // already https, skip
    return;
  }

  let localhost = localhostPatterns.some(function (item) {
    return url.startsWith(item);
  });

  /*localhostPatterns.forEach(item => {
    if (url.startsWith(item) === true) {
      localhost = true;
    }
  });*/

  if (localhost === true) {
    return;
  }

  let redirectUrl = url.replace(httpStartPattern, httpsStart);

  return { redirectUrl };
}

chrome.webRequest.onBeforeRequest.addListener(before_request, httpFilter, [
  "blocking"
]);
