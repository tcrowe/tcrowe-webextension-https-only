/*

Normally the defaults for prettier are okay but each tool that uses
prettier sets their own configuration. Now we must specify every
setting insted of relying on the defaults.

*/

module.exports = {
  printWidth: 80,
  tabWidth: 2,
  singleQuote: false,
  trailingComma: "none",
  bracketSpacing: true,
  semi: true,
  requirePragma: false,
  proseWrap: "never",
  arrowParens: "avoid",
  quoteProps: "as-needed",
  vueIndentScriptAndStyle: false,
  embeddedLanguageFormatting: "auto"
};
